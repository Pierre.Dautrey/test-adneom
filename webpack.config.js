let Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .addEntry('main', './assets/js/main.js')
    .addStyleEntry('style', './assets/css/style.css')
    .enableVueLoader()
;

module.exports = Encore.getWebpackConfig();