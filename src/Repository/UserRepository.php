<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    protected $entity = User::class;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return array
     * Renvoie un tableau de tous les utilisateurs ayant un ou plusieurs films préférés
     */
    public function getUsersWithFav() {

        $usersWithFav = $this->createQueryBuilder('u')
                     ->join('App:Vote','v','WITH','v.user = u.id')
                     ->join('App:Movie','m','WITH','v.movie = m.id')
                     ->orderBy('u.pseudo')
                     ->getQuery()
                     ->getResult();

        return $usersWithFav;
    }
}
