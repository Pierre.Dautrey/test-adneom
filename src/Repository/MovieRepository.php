<?php

namespace App\Repository;

use App\Entity\Movie;
use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Throwable;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    /**
     * @return array
     * Renvoie un tableau de tous les utilisateurs ayant un ou plusieurs films préférés
     */
    public function getBestMovie()
    {
        $movie = $this->createQueryBuilder('m')
            ->join('App:Vote', 'v', 'WITH', 'v.movie = m.id')
            ->select('m.name')
            ->groupBy('v.movie')
            ->orderBy('COUNT(v.movie)', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getResult();

        return $movie;
    }

    /**
     * @param $id
     * @return array
     * Renvoie un tableau de tous les utilisateurs ayant un ou plusieurs films préférés
     */
    public function getFavMovies($id)
    {
        $movie = $this->createQueryBuilder('m')
            ->join('App:Vote', 'v', 'WITH', 'v.movie = m.id')
            ->join('App:User', 'u', 'WITH', 'v.user = u.id')
            ->select('m.name, m.id, m.rate')
            ->where('u.id=:id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();

        if(!empty($movie)){
            return $movie;
        }

        return [];
    }

    /**
     * @param $name
     * @return array
     * Renvoie l'entité Movie liée au titre passé en parametre
     */
    public function getMovieByName($name)
    {
        $movie = $this->createQueryBuilder('m')
            ->where('m.name=:name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getResult();

        if(!empty($movie)){
            return $movie;
        }

        return [];
    }
}
