<?php

namespace App\Repository;

use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Vote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vote[]    findAll()
 * @method Vote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vote::class);
    }

    /**
     * @param $idUser
     * @param $idMovie
     * Permet de récupérer le vote correspondant à un lien user/movie
     * @return bool|\Exception|\Throwable
     */
    public function getVote($idUser, $idMovie){
        try {
            return $this->createQueryBuilder('v')
                ->where('v.user = :idUser')
                ->andWhere('v.movie = :idMovie')
                ->setParameter('idUser',$idUser)
                ->setParameter('idMovie',$idMovie)
                ->getQuery()
                ->getResult()[0];

        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $idUser
     * @param $idMovie
     * Permet de récupérer les votes correspondant à un user
     * @return bool|\Exception|\Throwable
     */
    public function getUserVote($idUser){
        try {
            return $this->createQueryBuilder('v')
                ->where('v.user = :idUser')
                ->setParameter('idUser',$idUser)
                ->getQuery()
                ->getResult();

        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }


}
