<?php

namespace App\Service;

use App\Entity\Movie;
use App\Entity\User;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

class MovieService {

    private $repository;
    private $voteService;
    private $em;

    public function __construct(MovieRepository $repository, VoteService $voteService, EntityManagerInterface $em) {
        $this->repository = $repository;
        $this->voteService = $voteService;
        $this->em = $em;
    }

    /**
     * @param $newMovie
     * @param $idUser
     * @return array Renvoie un tableau de tous les utilisateurs ayant un ou plusieurs films préférés
     * Renvoie un tableau de tous les films préfés d'un utilisateur après avoir ajouter un vote
     */
    public function addFavMovies($newMovie, $idUser)
    {
        $savedMovie = 0;

        try {
            $id = $newMovie['imdbID'];
            $movies = $this->repository->findAll();

            $user = $this->em->getRepository(User::class)->find($idUser);

            // On cherche si le film existe déjà en base
            foreach($movies as $movie) {
                if ($movie->getIdIMDB() == $id) {
                    $res = $this->voteService->insertVote($user, $movie);
                    $savedMovie = 1;
                    break;
                }
            }

            // Si le film n'héxiste pas en base, on l'ajoute avant d'ajouter le vote
            if(!$savedMovie){
                $movie = $this->addMovie($newMovie);
                $res = $this->voteService->insertVote($user, $movie);
            }

            $res = $this->repository->getFavMovies($idUser);
        } catch(Throwable $e) {
            return array($e);
        }

        return $res;
    }


    /**
     * @param $newMovie
     * Permet d'ajouter un film en base de donnée
     * @return Movie | string
     */
    public function addMovie($newMovie) {
        try {
            $movie = new Movie();
            $movie
                ->setDirector($newMovie['Director'])
                ->setIdIMDB($newMovie['imdbID'])
                ->setName($newMovie['Title'])
                ->setRate($newMovie['imdbRating']);

            $this->em->persist($movie);
            $this->em->flush();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
        return $movie;
    }

    /**
     * @return array
     * Renvoie un tableau de tous les utilisateurs ayant un ou plusieurs films préférés
     */
    public function getBestMovie()
    {
        return $this->repository->getBestMovie();
    }

    /**
     * @param $id
     * @return array
     * Renvoie un tableau de tous les films préférés d'un utilisateur
     */
    public function getFavMovies($id)
    {
        return $this->repository->getFavMovies($id);
    }

    /**
     * @param $name
     * @return array
     * Renvoie l'entité Movie liée au titre passé en parametre
     */
    public function getMovieByName($name)
    {
        $name = str_replace('+', ' ', $name);
        return $this->repository->getMovieByName($name);
    }

}