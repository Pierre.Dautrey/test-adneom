<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private $em;
    private $repository;

    public function __construct(UserRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * Supprime un utilisateur
     * @param $user
     * @return string
     */
    public function delete($user)
    {
        try{
            $this->em->remove($user);
            $this->em->flush();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
        return true;
    }


    /**
     * Supprime un utilisateur
     * @param $user
     * @return string
     */
    public function findAll()
    {
        try{
            return $this->repository->findAll();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return array
     * Renvoie un tableau de tous les utilisateurs ayant un ou plusieurs films préférés
     */
    public function getUsersWithFav() {
        return $this->repository->getUsersWithFav();
    }

    /**
     * @param $pseudo
     * @param $email
     * @param $dob
     * @param $validator
     * @return mixed
     * @throws \Exception
     */
    public function createUser($pseudo, $email, $dob, $validator) {
        $error = 'none';
        $user = new User();

        // Formattage des dates
        $dbo = new DateTime();
        $dbo->setDate(substr($dob,4,4),substr($dob,2,2),substr($dob,0,2));

        $dinsc = new DateTime();

        // Paramétrage de l'entité
        $user->setDateNaiss($dbo);
        $user->setDateInsc($dinsc);
        $user->setPseudo($pseudo);
        $user->setEmail($email);

        // Validation de l'entité
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $error = '';
            foreach ($errors as $err){
                $error = $error . $err->getMessage().' . ';
            }
            return $error;
        }

        // Création de l'entité
        $this->em->persist($user);
        $this->em->flush();

        return $error;
    }
}