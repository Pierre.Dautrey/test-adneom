<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Throwable;

class BestMovieHelper
{

    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private $client;
    private $imdbKey;

    public function __construct($imdbKey)
    {
        $this->client = HttpClient::create();
        $this->imdbKey = $imdbKey;
    }

    /**
     * @param $movie
     * @return mixed|string
     */
    public function getMovie($movie)
    {
        try {
            $response = $this->client->request('GET', 'http://www.omdbapi.com/?apikey=' . $this->imdbKey . '=' . $movie);
            $res = json_decode($response->getContent(), true);
        } catch (Throwable $e) {
            $res = "Exception : " . $e->getMessage();
        }

        return $res;
    }

    /**
     * @param $res
     * @param $movies
     * @return bool
     * TODO : Utiliser le component validator
     */
    public function isAddable($res,$movies)
    {
        // On vérifie si le film a bien un titre (provenant de l'API) et si l'utilisateur n'a pas déjà trois films
        if(!in_array($res['Title'],$movies) && count($movies) < 3){
            return true;
        }

        return false;
    }
}