<?php

namespace App\Service;

use App\Entity\Vote;
use App\Repository\VoteRepository;
use Doctrine\ORM\EntityManagerInterface;

class VoteService
{
    private $em;
    private $repository;

    public function __construct(VoteRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @param $user
     * @param $movie
     * @return bool|\Exception|\Throwable
     */
    public function insertVote($user, $movie){
        try {
            $vote = new Vote();
            $vote->setMovie($movie)
                ->setUser($user);

            $this->em->persist($vote);
            $this->em->flush();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
        return true;
    }

    /**
     * @param $idUser
     * Permet de suprimer les votes liés à un utilisateur
     * @return \Exception|mixed|\Throwable
     */
    public function deleteVote($idUser){
        try {
            $userVotes = $this->getUserVote($idUser);
            foreach ($userVotes as $vote) {
                $this->em->remove($vote);
                $this->em->flush();
            }
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $vote
     * Permet de suprimer un vote
     * @return \Exception|mixed|\Throwable
     */
    public function delete($vote){
        try {
            $this->em->remove($vote);
            $this->em->flush();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $idUser
     * @param $idMovie
     * Permet de récupérer le vote correspondant à un lien user/movie
     * @return bool|\Exception|\Throwable
     */
    public function getVote($idUser, $idMovie){
        return $this->repository->getVote($idUser, $idMovie);
    }

    /**
     * @param $idUser
     * @param $idMovie
     * Permet de récupérer les votes correspondant à un user
     * @return bool|\Exception|\Throwable
     */
    public function getUserVote($idUser){
        return $this->repository->getUserVote($idUser);
    }
}