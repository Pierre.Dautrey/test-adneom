<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200206172711 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "Migration créée à partir d'un git diff apres l'ajout des annotations sur les entities";
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user RENAME INDEX pseudo TO UNIQ_8D93D64986CC499D');
        $this->addSql('ALTER TABLE vote CHANGE id_movie id_movie INT DEFAULT NULL, CHANGE id_user id_user INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A108564F3DBB35F FOREIGN KEY (id_movie) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085646B3CA4B FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_5A108564F3DBB35F ON vote (id_movie)');
        $this->addSql('CREATE INDEX IDX_5A1085646B3CA4B ON vote (id_user)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d64986cc499d TO pseudo');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A108564F3DBB35F');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A1085646B3CA4B');
        $this->addSql('DROP INDEX IDX_5A108564F3DBB35F ON vote');
        $this->addSql('DROP INDEX IDX_5A1085646B3CA4B ON vote');
        $this->addSql('ALTER TABLE vote CHANGE id_movie id_movie INT NOT NULL, CHANGE id_user id_user INT NOT NULL');
    }
}
