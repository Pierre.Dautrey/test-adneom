<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200110132202 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO user VALUES (1,'test1','test1@test.fr',CURRENT_DATE,CURRENT_DATE,'','a:0:{}')");
        $this->addSql("INSERT INTO user VALUES (2,'test2','test2@test.fr',CURRENT_DATE,CURRENT_DATE,'','a:1:{i:0;s:12:\"Pulp Fiction\";}')");
        $this->addSql("INSERT INTO user VALUES (3,'test3','test3@test.fr',CURRENT_DATE,CURRENT_DATE,'','a:2:{i:0;s:12:\"Pulp Fiction\";i:1;s:12:\"The Revenant\";}')");
        $this->addSql("INSERT INTO user VALUES (4,'test4','test4@test.fr',CURRENT_DATE,CURRENT_DATE,'','a:3:{i:0;s:12:\"Pulp Fiction\";i:1;s:12:\"The Revenant\";i:2;s:17:\"Kill Bill: Vol. 1\";}')");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM user");

    }
}
