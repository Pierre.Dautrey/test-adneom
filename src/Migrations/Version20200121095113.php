<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200121095113 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE user ADD UNIQUE INDEX pseudo (pseudo)");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE user DROP INDEX pseudo");
    }
}
