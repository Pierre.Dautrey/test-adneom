<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200212125955 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A1085646B3CA4B');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A108564F3DBB35F');
        $this->addSql('DROP INDEX IDX_5A108564F3DBB35F ON vote');
        $this->addSql('DROP INDEX IDX_5A1085646B3CA4B ON vote');
        $this->addSql('ALTER TABLE vote ADD user_id INT NOT NULL, ADD movie_id INT NOT NULL, DROP id_movie, DROP id_user');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A108564A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085648F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('CREATE INDEX IDX_5A108564A76ED395 ON vote (user_id)');
        $this->addSql('CREATE INDEX IDX_5A1085648F93B6FC ON vote (movie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A108564A76ED395');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A1085648F93B6FC');
        $this->addSql('DROP INDEX IDX_5A108564A76ED395 ON vote');
        $this->addSql('DROP INDEX IDX_5A1085648F93B6FC ON vote');
        $this->addSql('ALTER TABLE vote ADD id_movie INT DEFAULT NULL, ADD id_user INT DEFAULT NULL, DROP user_id, DROP movie_id');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085646B3CA4B FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A108564F3DBB35F FOREIGN KEY (id_movie) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_5A108564F3DBB35F ON vote (id_movie)');
        $this->addSql('CREATE INDEX IDX_5A1085646B3CA4B ON vote (id_user)');
    }
}
