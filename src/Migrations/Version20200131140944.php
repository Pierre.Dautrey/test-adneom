<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131140944 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Modification complète de la structuration des données';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE user DROP film_pref");
        $this->addSql("ALTER TABLE user DROP film_prefs");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE user ADD film_pref VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD film_prefs LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }
}
