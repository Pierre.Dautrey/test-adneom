<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\BestMovieHelper;
use App\Service\MovieService;
use App\Service\UserService;
use App\Service\VoteService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

/**
 * @Route("/api")
 */
class UserApiController extends AbstractController
{

    private $movieService;
    private $userService;
    private $voteService;
    private $helper;

    public function __construct(MovieService $movieService, UserService $userService, VoteService $voteService, BestMovieHelper $helper)
    {
        $this->movieService = $movieService;
        $this->userService = $userService;
        $this->voteService = $voteService;
        $this->helper = $helper;
    }

    /**
     * Renvoie les informations des utilisateurs ayant une liste de film préférés
     * @Route("/users/withFav", name="user_with_pref", methods={"GET"})
     * @return JsonResponse
     */
    public function getUserWithFav()
    {
        $error = 'none';
        $status = 200;
        $data = [];

        try {
            $data = $this->userService->getUsersWithFav() ?? [];
            if(empty($data)) {
                $error = "Aucun utilisateur n'a de films preferes.";
                $status = 404;
            }
        } catch (Throwable $e) {
            $error = "Exception : " . $e->getMessage();
            $status = 400;
        }

        return $this->json(compact("error","data"),$status);
    }

    /**
     * Renvoie les informations des utilisateurs ayant une liste de film préférés
     * @Route("/bestMovie", name="best_movie", methods={"GET"})
     * @return JsonResponse
     */
    public function getBestMovie()
    {
        $status = 200;
        $error = "none";
        $data = [];

        try {
            $data = $this->movieService->getBestMovie();
            if(empty($data)) {
                $status = 404;
                $error = "Il n'y a pas de film gagnant actuellement";
            }
        } catch (Throwable $e) {
            $error = "Exception : " . $e->getMessage();
            $status = 400;
        }

        return $this->json(compact("error","data"),$status);
    }

    /**
     * Renvoie la liste des utilisateurs après création du nouveau
     * @Route("/users", name="create_user", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function createUser(Request $request, ValidatorInterface $validator) {

        $data = [];
        $error = '';
        $status = 400;

        try {
            // Récupération du body de la requête
            $requestData = json_decode($request->getContent());

            $pseudo = $requestData->pseudo;
            $email = $requestData->email;
            $dob = $requestData->DoB;

            $res = $this->userService->createUser($pseudo,$email,$dob,$validator);
            if($res === 'none'){
                $status = 201;
            } else {
                $error = $res;
            }

            // Récupération des utilisateurs existants pour la Response
            $data = $this->userService->findAll();
        } catch (UniqueConstraintViolationException $ex) {
            $status = 409;
            $error = "Le pseudo ou l'adresse mail est déjà utilisée.";
        } catch (Throwable $e) {
            $error = "Exception : " . $e->getMessage();
            $status = 400;
        }

        return $this->json(compact("error","data"),$status);
    }

    /**
     * Renvoie les informations d'un utilisateur à partir de son id
     * @Route("/users/{id}", name="user_show", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        $data =[];
        try {
            $data = array_values($this->movieService->getFavMovies($id));
            $error = "none";
            $status = 200;
        } catch(Throwable $e){
            $error = "Une erreur inconnue est survenue";
            $data = "Exception : " . $e->getMessage();
            $status = 400;
        }

        if(empty($data)){
            throw new NotFoundHttpException("Il n'existe pas d'utilisateur ayant cet identifiant.");
        }

        return $this->json(compact("error","data"),$status);

    }

    /**
     * Renvoie les informations d'un utilisateur après avoir ajouté un film à sa liste si possible
     * @Route("/users/movies", name="add_movie", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function addMovie(Request $request)
    {
        $data = null;
        $status = 201;
        $error = "L'utilisateur n'existe pas.";

        try {
            // Récupération du body de la requête
            $requestData = json_decode($request->getContent());

            $movie = $requestData->movie;
            $id = $requestData->selectedId;

            // Récupération des films préférés
            $favMovies = $this->movieService->getFavMovies($id);
            $movies = array_column($favMovies,"name");

            // Récupération du film depuis l'API
            $res = $this->helper->getMovie($movie);

            // Vérification de l'existance du film
            if(isset($res['Title'])) {
                if($this->helper->isAddable($res,$movies)){
                    $error = 'none';
                    $data = $this->movieService->addFavMovies($res,$id);
                } else {
                    $error = "Le film est deja dans la liste ou le maximum de film est atteint (3)";
                    $status = 400;
                }
            } else {
                $error = "Le film est introuvable";
                $status = 404;
            }
        } catch (Throwable $e) {
            $data = "Exception : " . $e->getMessage();
            $status = 400;
        }

        return $this->json(compact("error","data"),$status);
    }

    /**
     * Renvoie les informations de tous les utilisateurs
     * @Route("/users", name="get_all_users", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllUsers()
    {
        $data = [];
        $error = "none";
        $status = 200;

        try {
            $data = $this->userService->findAll();
        } catch (Throwable $e) {
            $error = "Exception : " . $e->getMessage();
            $status = 400;
        }

        return $this->json(compact("error","data"),$status);
    }

    /**
     * Renvoie la liste des films préférés après suppression de celui passé en paramètre
     * @Route("/users/{id}/{favMovie}", name="del_film_pref", methods={"DELETE"})
     * @param User $user
     * @param string $favMovie
     * @return RedirectResponse|Response
     */
    public function deleteFavMovie(User $user, string $favMovie)
    {
        $status = 200;
        $error = 'none';

        try {
            // Récupération de l'objet Movie concerné
            $movie = $this->movieService->getMovieByName($favMovie)[0];
            // Récupération du Vote lié au Movie
            $vote = $this->voteService->getVote($user->getId(),$movie->getId());

            if (!empty($vote)){
                $this->voteService->delete($vote);
            } else {
                $error = "Le film n'est pas dans la liste.";
                $status = 400;
            }
            $data = $this->movieService->getFavMovies($user->getId());
        } catch(Throwable $e){
            $data = "Exception : " . $e->getMessage();
        }

        return $this->json(compact("error","data"),$status);
    }

    /**
     * Attention : Pas de protection CSRF
     * Renvoi la liste des utilisateurs après suppression de celui passé en paramètre
     * @Route("/users/{id}", name="user_delete", methods={"DELETE"})
     * @param User $user
     * @return Response
     */
    public function delete(User $user): Response
    {
        $error = "none";
        $status = 200;
        $idUser = $user->getId();
        try{
            // On supprime d'abord les votes pour éviter les blocages de clés étrangères
            $res = $this->voteService->deleteVote($idUser);
            $res = $this->userService->delete($user);
            $data = $this->userService->findAll();
        } catch(Throwable $e){
            $data = "Exception : " . $e->getMessage();
            $error = "Impossible de supprimer cet utilisateur.";
            $status = 400;
        }

        return $this->json(compact("error","data"),$status);
    }
}
