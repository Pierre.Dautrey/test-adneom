<?php

declare(strict_types=1);

namespace App\Exception;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use function strpos;

/**
 * Class HTTPExceptionListener
 * @package App\Exception
 */
final class HTTPExceptionListener
{
    private $env;

    public function __construct($env)
    {
        $this->env = $env;
    }

    /**
     * Ce listener permet de renvoyer les exceptions au format json pour les erreurs serveur (les autres exceptions sont gérés dans le controller)
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getException();
        if(strpos($event->getRequest()->getRequestUri(), '/api/') === false){
            return;
        }

        if (! ($exception instanceof HttpException)) {
            if($this->env === "dev" && $exception instanceof NotEncodableValueException) {
                $event->setResponse(new JsonResponse(['error' => "La base de donnée n'est pas accessible",'data' => $exception->getMessage()],504));
            } elseif($this->env === "dev") {
                $event->setResponse(new JsonResponse(['error' => 'Le serveur ne sait pas répondre à votre requête','data' => $exception->getMessage()],504));
            } elseif($this->env === "dev" && $exception instanceof UniqueConstraintViolationException) {
                $event->setResponse(new JsonResponse(['error' => "Non respect des clés d'unicité, le pseudo ou l'adresse email existent déjà.",'data' => $exception->getMessage()],403));
            } else {
                $event->setResponse(new JsonResponse(['error' => 'Le serveur ne sait pas répondre à votre requête, la base de donnée semble innaccessible','data' => 'Environnement de production'],504));
            }
            return;
        } else {
            if($this->env === "dev" && $exception instanceof NotFoundHttpException){
                $event->setResponse(new JsonResponse(['error' => "L'utilisateur recherché n'a pas été trouvé",'data' => $exception->getMessage()],404));
            } elseif ($this->env === "dev" && $exception instanceof BadRequestHttpException) {
                $event->setResponse(new JsonResponse(['error' => "La requête envoyée est incorrecte",'data' => $exception->getMessage()],400));
            } elseif ($this->env === "dev" && $exception instanceof AccessDeniedHttpException) {
                $event->setResponse(new JsonResponse(['error' => "L'accès à cette ressource n'est pas accordé à votre session",'data' => $exception->getMessage()],403));
            } else {
                $event->setResponse(new JsonResponse(['error' => 'Le serveur ne sait pas répondre à votre requête, la requête Http semble invalide.','data' => 'Environnement de production'],504));
            }
            return;
        }
    }
}