# README

> - Cette application répond aux demandes du test BestMovie de DigiSchool Group
> - Elle est sous forme de SPA avec un back symfony et un front vue.js : branche master
> - J'ai au préalable réalisé une version avec twig qui n'est pas sous la forme d'une SPA : branche digischool.
> - J'ai également développé une version de la SPA avec une seule entity User, avec toute l'information stockée dans une table unique : branche SPA

# A Faire pour lancer le projet :

- 1: `git clone https://gitlab.com/Pierre.Dautrey/test-adneom.git`
- 2: `composer install`
- 3: `php bin/console server:run`

- 4: Il faut également créer une base de données sur une session locale qui s'appelle **test_adneom** (je n'ai pas inclus sa création aux migrations)
- 5: Adapter le paramétrage du .env (non gité) à votre BDD local et à l'API KEY :
    - Pour ma part j'ai utilisé laragon qui me fait tourner mysql accessible avec cette chaine de connection pour le mdp root:root (SET PASSWORD FOR root@localhost=PASSWORD('root'))
    - `DATABASE_URL=mysql://root:root@127.0.0.1:3306/test_adneom?serverVersion=5.7`
    - `API_KEY=70f18f5d&t`
- 6: Enfin il faut jouer les migrations :
    - `php bin/console doctrine:migrations:migrate`
- 7: Ainsi qu'installer les packages npm :
    - `npm install`
    - `npm run watch`
- Si il y a un problème avec webpack : `composer require symfony/webpack-encore-bundle`
- Si il y a une différence entre les entités et la BDD : `php bin/console doctrine:migrations:diff` puis jouer la migration générée.

Il suffit alors d'accéder à l'URL suivante : **http://127.0.0.1:8000/home**

# les routes disponibles de l'API sont :
	
- GET : `/api/users/{id}`
	- Renvoie les informations d'un utilisateur à partir de son id (dont le choix de films)
- GET : `/api/users`
    - Renvoie la liste de tous les utilisateurs et leurs informations
- GET : `/api/users/withPref`
    - Renvoie la liste de tous les utilisateurs ayant une liste de film préférés  
- GET : `/api/bestMovie`
    - Renvoie le film présent dans le plus de listes   
- DELETE : `/api/users/{id}/{filmPref}`
	- Renvoie la liste des films préférés après **suppression** de celui passé en paramètre
- DELETE : `/api/users/{id}`
	- Renvoie la liste des utilisateurs après **suppression** de celui passé en paramètre
- POST : `/api/users`
    - Renvoie la liste des utilisateurs après avoir **créé** le nouveau 
    - Body : pseudo / email / DoB (Date au format : **DDMMYYYY**)
- POST : `/api/users/movies`
    - Renvoie les informations d'un utilisateur après avoir **ajouté** un film à sa liste
    - Body : selectedId / movie
    
# Les choix techniques et spécificités :

- Les affiches de film sont sur une API payante maintenant

- Utilisation du package HttpClient de Symfony (Symfony\Component\HttpClient\HttpClient) très pratique pour crééer des requêtes HTTP 

- Bien que le fait d'avoir un fichier de routing permette une organisation plus claire de par la séparation des concepts, j'ai préferé utiliser les annotations qui rendent plus simple la lecture et la modification des routes. Pour ce développement les routes sont simples et pas vouées à être réutilisées par d'autre développeurs qui voudraient établir leur propre version du Routing.

- Comme j'ai développé en Symfony 4.* je n'ai pas utilisé la notion de Bundle. En effet cette dernière n'est plus recommandée par Symfony sauf dans le cas ou les Bundle doivent être partagés entre plusieurs applications.

- L'utilisation de FosRestBundle ou de API Platfrom auraient facilité la création d'une API RESTFUL, étant partit sur la réalisation d'une application Front/Back à la base et connaissant peu ces librairies je n'ai pas eu le temps de les mettre en place.

- J'ai choisit Vue.js pour faire un front rapide et dynamique car je connais ce langage et il permet rapidement d'avoir de hautes performances.

- Le validator permet de s'assurer que l'email est bien un email (en accès via API) et la vérification côté Front est toujours active pour éviter une requête inutile.

- Pour la gestion des champs created_at et updated_at j'ai utilisé l'extension : stof_doctrine_extensions qui permet de gérer cela avec une simple annotation.